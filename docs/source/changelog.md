# Changelog

Les changements notables du projets sont documentés dans ce fichier

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
Ce projet suit le [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## À venir

### Ajouts

### Changements

### Dépréciations

### Suppressions

### Corrections

### Sécurité

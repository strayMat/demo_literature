# Usage

## Requirements

- TODO



# Utilisation

- TODO

## Execution du projet

> 📝 **Note**
> Toutes les commandes qui suivent sont exécutées à la _racine_ du projet.
> On suppose que `make` est installé sur le système

On peut executer ce projet ainsi:

### Localement en utilisant poetry

Par exemple :

```shell script
make provision-environment # Note: installe toutes les dépendances !
poetry shell # Active le virtualenv du projet
cli main # Utilise la CLI du projet
```



# Développement

> 📝 **Note**
> Une partie des étapes ci-dessous, sont encapsulées dans le `Makefile` pour faciliter leur utilisation.

> 🔥 **Tip**
> En invoquant `make` sans aucun argument, une liste des commandes disponibles sera affichée.

## Packets et gestion des dépendances

Assurez-vous d'avoir `Python 3.8` et [poetry](https://python-poetry.org/) installés configurés.

Pour installer le projet ainsi que toutes les dépendances (incluant celles de dev):

```shell script
make provision-environment
```

> 🔥 **Tip**
> Exécuter cette commande sans poetry affichera un message d'erreur utile, vous indiquant de l'installer.





## Tests

On utilise [pytest](https://pytest.readthedocs.io/) pour les tests unitaires.

Pour lancer les tests, utilisez :

```shell script
make test
```



## Qualité du code

On utilise [pre-commit](https://pre-commit.com/) pour orchestrer les différentes étapes de linting.

Pour lancer le linting, utilisez :

```shell script
make lint
```

> 🚨 **Danger**
> La CI cassera si les tests ou le linting ne passent pas.
> Il est donc recommandé d'executer les checks avant de pousser les changements.

### Automatisation avec les pre-commit-hooks

Pour lancer le linting avant chaque commit, utilisez:

```shell script
make install-pre-commit-hooks
```

> ⚠️ Attention !
> Si les vérifications ne passent pas, cela empêchera le commit.
> Certains checks peuvent modifier automatiquement des fichiers (`isort` et `black` notamment).
> Dans ce cas, il suffit de stagé les changements et de refaire `git commit`



## Documentation

Pour générer la documentation, utilisez :

```shell script
make docs-clean docs-html
```

> 📝 **Note**
> Cette commande va générer des fichiers html dans `docs/_build/html`.
> La page d'accueil de la doc est `docs/_build/html/index.html`.

#! /usr/bin/env python
import logging

import click
from dotenv import load_dotenv

from demo_literature.constants import LOG_LEVEL
from demo_literature.utils import hello

# see `.env` for requisite environment variables
load_dotenv()


logging.basicConfig(
    level=logging.getLevelName(LOG_LEVEL),
    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
)


@click.group()
@click.version_option(package_name="demo_literature")
def cli():
    pass


@cli.command()
def main() -> None:
    """demo_literature Main entrypoint"""
    click.secho(hello(), fg="green")


if __name__ == "__main__":
    cli()
